<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');//為了引入css,js等外部元件
		$this->load->view('welcome_page');//means: views/welcome_page.php
	}
	
	public function aboutUs(){
		
		$this->load->helper('url');//為了引入css,js等外部元件
		$this->load->view('aboutUs');//means: views/aboutUs.php
		
	}
}

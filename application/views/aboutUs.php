<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?>
<!DOCTYPE HTML>

<html>
<head>
<base href="<?php echo base_url() . 'application/views/';?>" />
<title>.::TrendPlus 趨勢家::. about us</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
<?php include("includePage.php"); ?>
</head>
<body class="no-sidebar">
	<div id="page-wrapper">

		<!-- Header -->
		<div id="header">

			<!-- Inner -->
			<div class="inner">
				<header>
					<h1>
						<a href="index.html" id="logo">TrendPlus 趨勢家</a>
					</h1>
				</header>
			</div>

			<!-- Nav -->
			<?php include("nav.php"); ?>

		</div>

		<!-- Main -->
		<div class="wrapper style1">

			<div class="container">
				<article id="main" class="special">
					<header>
						<h2 style="color:#ef8376;">ABOUT US</h2>
					</header>
					<p>從大量社群網站，新聞媒體，熱門討論區及部落格擷取各式文章及評論，依據合作的相關企業選出特定主題品牌或產業口碑資訊，透過文章中的文字，萃取主題特徵進而轉換成有價值的訊息，協助企業掌握市場趨勢，提升競爭力
					</p>
				</article>
				<hr />
				<div class="row">
					<article class="4u 12u(mobile) special">
						<a href="#" class="image featured"><img src="images/pic09.jpg"
							alt="" /></a>
						<header>
							<h3 style="color:#ef8376;">
								Vincent
							</h3>
						</header>
						<p>科專顧問</p>
					</article>
					<article class="4u 12u(mobile) special">
						<a href="#" class="image featured"><img src="images/pic09.jpg"
							alt="" /></a>
						<header>
							<h3 style="color:#ef8376;">
								NiNi
							</h3>
						</header>
						<p>元智大學資訊管理研究所所碩士</p>
					</article>
					<article class="4u 12u(mobile) special">
						<a href="#" class="image featured"><img src="images/pic09.jpg"
							alt="" /></a>
						<header>
							<h3 style="color:#ef8376;">
								Isabella
							</h3>
						</header>
						<p>輔仁大學企業管理學系</p>
					</article>
				</div>
				<div class="row">
					<article class="4u 12u(mobile) special">
						<a href="#" class="image featured"><img src="images/pic09.jpg"
							alt="" /></a>
						<header>
							<h3 style="color:#ef8376;">
								Kody
							</h3>
						</header>
						<p>市場分析</p>
					</article>
					<article class="4u 12u(mobile) special">
						<a href="#" class="image featured"><img src="images/pic09.jpg"
							alt="" /></a>
						<header>
							<h3 style="color:#ef8376;">
								Tracy
							</h3>
						</header>
						<p>Microsoft 行銷企劃專員</p>
					</article>
					<article class="4u 12u(mobile) special">
						<a href="#" class="image featured"><img src="images/pic09.jpg"
							alt="" /></a>
						<header>
							<h3 style="color:#ef8376;">
								Emma
							</h3>
						</header>
						<p>Microsoft 行銷企劃專員</p>
					</article>
				</div>
			</div>

		</div>

		<!-- Footer -->
		<?php include("includeFooter.php"); ?>

	</div>

</body>
</html>
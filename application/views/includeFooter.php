<div id="footer" >
	<div class="container">
		
		
		<div class="row">
			<div class="12u">

				<!-- Contact -->
					<section class="contact" >
						<header>
							<h3><font color='black'>聯絡我們</font></h3>
						</header>
						<ul class="icons"  align="left">
							<li><font color='black'>電話：0936-273792</font></li><br>
							<li><font color='black'>電郵：service@trendplus.cc</font></li>
						</ul>
					</section>

				<!-- Copyright -->
					<div class="copyright">
						<ul class="menu">
							<li><font color='black'>&copy;2017 TrendPlus.cc All rights reserved. Design: KentFan</font></li>
						</ul>
					</div>

			</div>

        </div>
    </div>
</div>
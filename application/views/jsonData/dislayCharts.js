$(function(){
        	//wordCloud();
        	pieChart();
        	semanticChart();
        	barChart();
        	lineChart();
        });
        
        function wordCloud() {
	        $.getJSON("jsonData/wordCloud.json", function(data) {
	            //console.log(JSON.stringify(data)); // this will show the info it in firebug console
	            var wordCloudData=[];
	    	    
	            //alert(data.wordCloud.length);
	        	for (var i=0 ; i<data.wordCloud.length; i++)
	        	{
	        		wordCloudData.push({text : data.wordCloud[i].keyword, size:data.wordCloud[i].weight});
	        	}
				
				//alert(JSON.stringify(wordCloudData));
	        	var fill = d3.scale.category20();   
	        	d3.layout.cloud().size([600, 400])   
	    	     .words(wordCloudData)   
	    	     .rotate(function() { return ~~(Math.random() *2) * 90; })   
	    	     .font("Impact")   
	    	     .fontSize(function(d) { return (d.size); })   //function(d) { return d.size; }
	    	     .on("end", draw)   
	    	     .start(); 

		    	function draw(words) {   
		    	  d3.select("#tag") //要插入標籤雲的tag id
	    	      .append("svg")   
	    	      .attr("width", 800)   
	    	      .attr("height", 400)   
	    	      .append("g")   
	    	      .attr("transform", "translate(400,200)") //這裡的值要對應到繪圖區域的寬
	    	      .selectAll("text")   
	    	      .data(words)   
	    	      .enter().append("text")   
	    	      .style("font-size", function(d) { return d.size + "px"; })   
	    	      .style("font-family", "Impact")   
	    	      .style("fill", function(d, i) { return fill(i); })   
	    	      .attr("text-anchor", "middle")   
	    	      .attr("transform", function(d) {
	    	        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";   
	    	       })   
	    	      .text(function(d) { return d.text; });   
		    	}
	        });
        }
        
        <!-- Pie chart -->
        function pieChart() {
            $.ajax({
                    type:'get',
                    url:'jsonData/pieChart.json',
                    dataType:'JSON',
                    data:{
                    },
                    success:function(data){
                    	//console.log(JSON.stringify(data));
                    	var dkhIdAndName=[], dkhAll=[], dkhId=[], dkhName=[];
                	    
                    	//1.前處理
                    	for (var i=0 ; i<data.rows.length; i++)
                    	{
                    		dkhAll[i] = data.rows[i].domainKnowhowId+","+data.rows[i].domainKnowhowName+","+data.rows[i].keyword+","+data.rows[i].keywordScore+","+data.rows[i].domainKnowhowScore;
                    		dkhIdAndName[i] = data.rows[i].domainKnowhowId+","+data.rows[i].domainKnowhowName;
                    		dkhName[i] = data.rows[i].domainKnowhowName;
                    	}
                    	
                    	//2.去除array中重複元素.
                    	dkhIdAndName = dkhIdAndName.filter(function (el, i, arr) {
                    		return arr.indexOf(el) === i;
                    	});

                    	//3.categories arrange and colors array
                    	var colors = Highcharts.getOptions().colors,
                    	i=0, categories=[], dataArr=[];
                    	
                    	for(i=0; i<dkhIdAndName.length; i++) {
                    		var dkhIdAndNameSplit = dkhIdAndName[i].split(",");
                    		var dkhId = dkhIdAndNameSplit[0];//domainKnowHowId
                    		categories.push(dkhIdAndNameSplit[1]);//domainKnowHowName
                    		var drilldowns=[], drilldowns2=[], keywords=[], keywordScore=[];
                    		var dkhIdHasSet=null;
                    		//2.arange data
                    		for(var j=0; j<dkhAll.length; j++) {
                    			
                    			//dkhAll_Array: 0:domainKnowhowId, 1:domainKnowhowName, 2:keyword, 3:keywordScore, 4:domainKnowhowScore
                    			var dkhAllSplit = dkhAll[j].split(",");
                    			
                    			if(dkhId===dkhAllSplit[0]) {
                    					
                    				drilldowns.push({categories:dkhAllSplit[2], data:Number(dkhAllSplit[3])});
                    				
                    				if(dkhIdHasSet==null) {
                    					dataArr.push({y:Number(dkhAllSplit[4]), color:colors[i], drilldown:drilldowns2});
                    					dkhIdHasSet = dkhId;
                    				}
                    			}
                    		}//end of for
                    		
                    		//整理drillData
                    		for(var d=0; d<drilldowns.length; d++) {
                    			keywords.push(drilldowns[d].categories);
                    			keywordScore.push(drilldowns[d].data);
                    		}//end of for
                    		drilldowns2.push({categories:keywords, data:keywordScore});
                    		//console.log(JSON.stringify(drilldowns2));
                    	}//end of for

                    	var data = dataArr,
                        browserData = [],
                        versionsData = [],
                        i,
                        j,
                        dataLen = data.length,
                        drillDataLen,
                        brightness;
						
                    	// Build the data arrays
                    	for (i = 0; i < dataLen; i += 1) {
                    	
                    	    // add browser data
                    	    browserData.push({
                    	        name: categories[i],
                    	        y: data[i].y,
                    	        color: data[i].color
                    	    });
                    	
                    	    // add version data
                    	    drillDataLen = data[i].drilldown[0].data.length;
                    	    for (j = 0; j < drillDataLen; j += 1) {
                    	        brightness = 0.2 - (j / drillDataLen) / 5;
                    	        versionsData.push({
                    	            name: data[i].drilldown[0].categories[j],
                    	            y: data[i].drilldown[0].data[j],
                    	            color: Highcharts.Color(data[i].color).brighten(brightness).get()
                    	        });
                    	    }//end of for
                    	}//end of for

                    	//start chart items
                    	// Create the chart
                    	Highcharts.chart('container_pieChart', {
                    	    chart: {
                    	        type: 'pie'
                    	    },
                    	    credits: {
                                enabled: false //移除highchats右下方logo.
                            },
                    	    title: {
                    	        text: ''
                    	    },
                    	    subtitle: {
                    	        text: ''
                    	    },
                    	    yAxis: {
                    	        title: {
                    	            text: 'Total percent market share'
                    	        }
                    	    },
                    	    plotOptions: {
                    	        pie: {
                    	            shadow: false,
                    	            center: ['50%', '50%']
                    	        }
                    	    },
                    	    tooltip: {
                    	        valueSuffix: '%'
                    	    },
                    	    series: [{
                    	        name: '佔比',
                    	        data: browserData,
                    	        size: '60%',
                    	        dataLabels: {
                    	            formatter: function () {
                    	                return this.y > 5 ? this.point.name : null;
                    	            },
                    	            color: '#ffff',
                    	            distance: -30,
                    	            style: {
                                        fontSize: '18px',
                                        fontFamily: 'Arial, sans-serif'
                                    }
                    	        }
                    	    }, {
                    	        name: '關鍵字比率',
                    	        data: versionsData,
                    	        size: '80%',
                    	        innerSize: '60%',
                    	        dataLabels: {
                    	            formatter: function () {
                    	                // display only if larger than 1
                    	                return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%' : null;
                    	            },
                    	            style: {
                                        fontSize: '13px',
                                        fontFamily: 'Arial, sans-serif'
                                    }
                    	        }
                    	    }]
                    	});//end of chart items
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){
                        alert("pie chart data error");
                    }

            });
        }//end of pieChart

        function semanticChart() {
            $.ajax({
                    type:'get',
                    url:'jsonData/semanticChart.json',
                    dataType:'JSON',
                    data:{
                    },
                    success:function(data){
                    	//console.log(JSON.stringify(data));
                    	var counter=[], domainKnowhow=[], totalScore=[];
 	    	    
		 	        	//1.前處理
		 	        	for (var i=0 ; i<data.rows.length; i++)
		 	        	{
		 	        		counter[i] = data.rows[i].counter;//該議題詞有幾個關鍵字(keyword)
		 	        	    domainKnowhow[i] = data.rows[i].domainKnowhowName;//議題詞
		 	        	    totalScore[i] = data.rows[i].totalScore;//分數
		 	        	}

		 	        	$('#container_semantic').highcharts({
		 	        		
		 	               chart: {
		 	                   zoomType: 'x'
		 	               },
		 	               credits: {
		 	                   enabled: false //移除highchats右下方logo.
		 	               },
		 	               title: {
		 	                   text: ''
		 	               },
		 	               subtitle: {
		 	                   text:  '' //show 
		 	               },
		 	               xAxis: [{
		 	                   categories: domainKnowhow, //議題詞
		 	                   crosshair: true
		 	               }],
		 	               yAxis: [{ // Primary yAxis
		 	                   labels: {
		 	                       format: '${value}',
		 	                       style: {
		 	                           color: Highcharts.getOptions().colors[0]
		 	                       }
		 	                   },
		 	                   title: {
		 	                       text: '關鍵字個數 (個)',
		 	                       style: {
		 	                           color: Highcharts.getOptions().colors[0]
		 	                       }
		 	                   }
		 	               }, { // Secondary yAxis
		 	                   title: {
		 	                       text: '情緒總分',
		 	                       style: {
		 	                           color: "red"
		 	                       }
		 	                   },
		 	                   labels: {
		 	                       format: '{value}',
		 	                       style: {
		 	                           color: "red"
		 	                       }
		 	                   },
		 	                   opposite: true
		 	               }],
		 	               tooltip: {
		 	                   shared: true
		 	               },
		 	               
		 	               legend: {
		 	                   layout: 'vertical',
		 	                   align: 'left',
		 	                   x: 0,
		 	                   verticalAlign: 'top',
		 	                   y: 0,
		 	                   floating: true,
		 	                   backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		 	               },
		 	               plotOptions: {
		 	               	column: {
		 	                       dataLabels: {
		 	                           enabled: true
		 	                       }
		 	                   },
		 	                   spline: {
		 	                       dataLabels: {
		 	                           enabled: true,
		 	                           color: "red"
		 	                       }
		 	                   }
		 	               },
		 	               
		 	               series: [{
		 	                   name: '關鍵字個數',
		 	                   type: 'column',
		 	                   yAxis: 0,
		 	                   data: counter, //個數
		 	                   color: Highcharts.getOptions().colors[0],
		 	                   tooltip: {
		 	                       valueSuffix: ''
		 	                   }

		 	               }, {
		 	                   name: '情緒總分',
		 	                   type: 'spline',
		 	                   color: "red",
		 	                   yAxis: 1,
		 	                   data: totalScore,//close
		 	                   tooltip: {
		 	                       valueSuffix: ''
		 	                   }
		 	               }]
		 	           });
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){
                        alert("semanticChart data error");
                    }

            });//end of ajax
        }//end of semanticChart


        //CHART JS 長條圖barChart.
		var bChart = null;
		var bOptions = {
	        chart: {
	        	renderTo: 'container_barChart', //设置显示的页面块
	            type: 'column',
	            zoomType: 'x'
	        },
	        credits: {
	            enabled: false //移除highchats右下方logo.
	        },
	        title: {
	            text: ''
	        },
	        subtitle: {
	            text: '',
	        },
	        xAxis: {
	            //categories: ['Q1','Q2','Q3'], //時間區間, 改用動態處理!
	            crosshair: true,
	        },
	        yAxis: [{
	        	     min: 0,
	                 title: {
	                   text: 'TF-Term Frequency (個)'
	                 }
	                },{
	        	     //min: 0,
	                 title: {
	                   text: 'QUANTITY'
	                 },
	                 opposite: true
	                }
	                ],
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y:.0f} 個</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	        	column: {
	                dataLabels: {
	                    enabled: true,
	                    rotation: -80,
	                    align: 'right',
	                    format: '{point.y:.0f}', // one decimal
	                    y: -18, // 10 pixels down from the top
	                    style: {
	                        fontSize: '9px',
	                        fontFamily: 'Arial, sans-serif'
	                    }
	                }
	            }
	        }
	        
		};//end of bOptions


        
        //get dynamic series.
        function barChart(){
        	$.ajax({
                type:'get',
                url:'jsonData/domainKnowhowAndPeriodStatistic.json',
                dataType:'JSON',
                data:{
                },
                success:function(data){
                	//console.log(JSON.stringify(data));
                	var period=[], domainKnowhow=[], TF=[], DF=[], seriesData=[], seriesLineData=[];
    	    
		        	//1.前處理
		        	for (var i=0 ; i<data.rows.length; i++)
		        	{
		        		period[i] = data.rows[i].periodName;
		        	    domainKnowhow[i] = data.rows[i].domainKnowhowName;
		        	}
		        	
		        	//2.去除array中重複元素.
		        	var dedupePeriod = period.filter(function (el, i, arr) {
		        		return arr.indexOf(el) === i;
		        	});
		        	
		        	var dedupeDomainKnowhow = domainKnowhow.filter(function (el, i, arr) {
		        		return arr.indexOf(el) === i;
		        	});
		        	
		        	//3.組新json給動態series for Bar chart.
		        	for(var i=0; i<dedupeDomainKnowhow.length; i++) {
		        		for(var j=0; j<dedupePeriod.length; j++) {
		        			var tfData=[];
		        			for(var k=0; k<data.rows.length; k++) {
		        				
		        				if(dedupeDomainKnowhow[i]==data.rows[k].domainKnowhowName){
		        					//alert(dedupePeriod[i]+', '+ dedupeDomainKnowhow[j] + ', ' + Number(data.rows[k].TF));
		        					tfData.push(Number(data.rows[k].TF));
		                		}
		        			}
		        		}
		        		seriesData.push({name : dedupeDomainKnowhow[i], data:tfData});
		        	}
		        	//4.异步添加第数据列
		            LoadSerie_barChart(dedupePeriod, seriesData);
                	
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){
                    alert("pie chart data error");
                }

            });//end of ajax
            
            bChart = new Highcharts.Chart(bOptions);
            
        };//end of barChart
        //長條圖：异步读取数据并加载到图表
        function LoadSerie_barChart(period, series) {
            bChart.showLoading();
            bChart.xAxis[0].setCategories(period);//添加X轴
            
            $.each(series, function(i, n) {
                var bSeries = {
                		yAxis: 0,
                
                        name: series[i].name,
                        data: series[i].data,
                        type: 'column',
                };
                bChart.addSeries(bSeries);//添加数据列
            });
            
            
            //進出貨量
            $.ajax({
      			type: 'POST',
      			url:'jsonData/inOutRecord.json',
      			dataType: "json",
      			success: function(data) {
      				
      			   if(data.inOutRecord==null)
      				   return;
      			   
      			   var inQuantity = new Array(period.length), 
      			       outQuantity = new Array(period.length);
      			   
      			   for(var i=0; i<period.length; i++) {
      				   for(var j=0; j<data.inOutRecord.length; j++) {
      				   	   if(period[i]==data.inOutRecord[j].periodName) {
      				   		  inQuantity[i] = data.inOutRecord[j].inQuantity;
      				   		  outQuantity[i] = data.inOutRecord[j].outQuantity;
      				   	   }
        			   }
      			   }
      			   
      			   for(var i=0; i<inQuantity.length; i++) {
      				   if(inQuantity[i] == null)
      					   inQuantity[i] = 0;  
      			   }
      			   for(var i=0; i<outQuantity.length; i++) {
    				   if(outQuantity[i] == null)
    					   outQuantity[i] = 0;  
    			   }
      			   

      		       bChart.xAxis[0].setCategories(period);//添加X轴
    	           var inQuantitySeries = {
    	        		   yAxis: 1,
    	                   name: "Predict",
    	                   data: inQuantity,
    	                   type: 'spline',
    	           };
      		       
    	           var outQuantitySeries = {
    	        		   yAxis: 1,
    	                   name: "Actual",
    	                   data: outQuantity,
    	                   type: 'spline',
    	           };
    	           
    	           bChart.addSeries(inQuantitySeries);//添加数据列
    	           bChart.addSeries(outQuantitySeries);//添加数据列
      			}
            });
            
            bChart.hideLoading();
        };

        function lineChart() {
            $.ajax({
                    type:'get',
                    url:'jsonData/domainKnowhowAndPeriodStatistic.json',
                    dataType:'JSON',
                    data:{
                    },
                    success:function(data){
                    	//console.log(JSON.stringify(data));
                    	var period=[], domainKnowhow=[], TF=[], DF=[], seriesData=[], seriesLineData=[];
                	    
                    	//1.前處理
                    	for (var i=0 ; i<data.rows.length; i++)
                    	{
                    		period[i] = data.rows[i].periodName;
                    	    domainKnowhow[i] = data.rows[i].domainKnowhowName;
                    	}
                    	
                    	//2.去除array中重複元素.
                    	var dedupePeriod = period.filter(function (el, i, arr) {
                    		return arr.indexOf(el) === i;
                    	});
                    	
                    	var dedupeDomainKnowhow = domainKnowhow.filter(function (el, i, arr) {
                    		return arr.indexOf(el) === i;
                    	});
                    	//4.組新json給動態series for Line chart.
                    	for(var i=0; i<dedupePeriod.length; i++) {
                    		for(var j=0; j<dedupeDomainKnowhow.length; j++) {
                    			var tfData=[];
                    			for(var k=0; k<data.rows.length; k++) {
                    				
                    				if(dedupePeriod[i]==data.rows[k].periodName){
                    					//alert(dedupePeriod[i]+', '+ dedupeDomainKnowhow[j] + ', ' + Number(data.rows[k].TF));
                    					tfData.push(Number(data.rows[k].TF));
                            		}
                    			}
                    		}
                    		seriesLineData.push({name : dedupePeriod[i], data:tfData});
                    	}
                    	//5 
                    	LoadSerie_lineChart(dedupeDomainKnowhow, seriesLineData);
                    	
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){
                        alert("pie chart data error");
                    }
            });//end of ajax
            lChart = new Highcharts.Chart(lOptions);
        }//end of lineChart

        //CHART JS 折線圖lineChart.
        var lChart = null;
        var lOptions = {
         //$('#container').highcharts({
             chart: {
             	renderTo: 'container_lineChart', //设置显示的页面块
                type: 'column',
                //zoomType: 'xy'
             },
             credits: {
                 enabled: false //移除highchats右下方logo.
             },
             title: {
                 text: ''
             },
             subtitle: {
                 text: '',
             },
             xAxis: {
                 //categories: ['Q1','Q2','Q3'], //時間區間, 改用動態處理!
                 crosshair: true,
             },
             yAxis: {
             	min: 0,
                 title: {
                     text: 'TF-Term Frequency (個)'
                 }
             },
             tooltip: {
                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                     '<td style="padding:0"><b>{point.y:.0f} 個</b></td></tr>',
                 footerFormat: '</table>',
                 shared: true,
                 useHTML: true
             },
             plotOptions: {
            	 spline: {
                     dataLabels: {
                         enabled: true,
                         style: {
                             fontSize: '9px',
                             fontFamily: 'Arial, sans-serif'
                         }
                     }
                 }
             }
         };

         //异步读取数据并加载到图表
         function LoadSerie_lineChart(domainKnowhow, series) {
             lChart.showLoading();
             lChart.xAxis[0].setCategories(domainKnowhow);//添加X轴
             
             $.each(series, function(i, n) {
                 var lSeries = {
                         name: series[i].name,
                         data: series[i].data,
                         type: 'spline',
                 };
                 lChart.addSeries(lSeries);//添加数据列
             });
             
         	/**
         	[{"name":"2012Q4","data":[21,0,7,14,15,57]},
         	 {"name":"2013Q1","data":[96,0,62,22,91,29]},
         	 {"name":"2014Q4","data":[24,0,13,5,39,0]}]
         	**/
             
             lChart.hideLoading();
         }
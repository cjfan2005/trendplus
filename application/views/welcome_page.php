<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>


<html lang="en">
	<head>
	    <base href="<?php echo base_url() . 'application/views/';?>" />
	    
	    <meta charset="utf-8">
		<title>.::TrendPlus 趨勢家::. </title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<?php include("includePage.php"); ?>
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><font id="logo" color="black">品牌行銷決策<br>口碑語意分析</font></h1>
								<hr />
								<p><font id="logo" color="black">蒐集⇢分析⇢觀察⇢發現⇢警示⇢決策</font></p>
							</header>
							<footer>
								<a href="#banner" class="button circled scrolly">Start</a>
							</footer>
						</div>

					<!-- Nav 選單列-->
					    
					    
				</div>

			<!-- Banner -->
				<section id="banner">
					<header>
						<h2>Hi. You're looking at <strong>Trend Plus </strong>!</h2>
						<p>
							領域專家、事件分析、視覺圖表、策略諮詢
						</p>
					</header>
				</section>

			<!-- Carousel -->
				<section class="carousel"><br>
					<div class="reel">

						<article>
							<a href="http://www.tonywear.com/" class="image featured" target="_blank"><img src="images/tonywear.png" height="256" width="256" alt="" /></a>
							<header>
								<h3><a href="http://www.tonywear.com/">服飾產業</a></h3>
							</header>
							<p></p>
						</article>
						
						<article>
							<a href="#" class="image featured" target="_blank"><img src="images/ecotech.png" height="256" width="100" alt="" /></a>
							<header>
								<h3><a href="#">環保科技</a></h3>
							</header>
							<p></p>
						</article>
						
						<article>
							<a href="#" class="image featured" target="_blank"><img src="images/jason.jpg" height="256" width="256" alt="" /></a>
							<header>
								<h3><a href="#">電信通訊</a></h3>
							</header>
							<p></p>
						</article>

						<article>
							<a href="#" class="image featured" target="_blank"><img src="images/hikari.jpg" height="256" width="256" alt="" /></a>
							<header>
								<h3><a href="#">生活用品</a></h3>
							</header>
							<p></p>
						</article>

						<article>
							<a href="#" class="image featured" target="_blank"><img src="images/tst.png" height="256" width="256" alt="" /></a>
							<header>
								<h3><a href="#">美妝商品</a></h3>
							</header>
							<p></p>
						</article>
						
						<footer>
							
						</footer>
					</div>
				</section>

			<!-- Main -->
				<div class="wrapper style2"><br>

					<article id="main" class="container special">
					    <a href="#" class="image featured"><img src="images/wordcloud.png" alt="" /></a>
						<!-- <div id="tag" style=" margin: 0 auto;"></div> -->
						<header>
							<h2 style="color:#ef8376;">輿情關鍵字</h2>
							<p>
								時事的觸動引發討論關鍵字
							</p>
						</header>
						<p>
						</p>
						
					</article>

				</div>
				
				<div class="wrapper style2"><br>

					<article id="main" class="container special">
						<div id="container_pieChart" style="width: 680px; height: 500px; margin: 0 auto; "></div>
						<header>
							<h2 style="color:#ef8376;">議題與關鍵字之關連性</h2>
							<p>
								熱門議題與品牌關鍵字之比重
							</p>
						</header>
						<p>
						</p>
						
					</article>

				</div>
				
				<div class="wrapper style2"><br>

					<article id="main" class="container special">
						<div id="container_semantic" style="min-width: 310px; height:500px; margin: 0 auto"></div>
						<header>
							<h2 style="color:#ef8376;">品牌口碑情緒分析</h2>
							<p>
								忠實反應網友意見，找出情緒對議題的影響
							</p>
						</header>
						<p>
							
						</p>
						
					</article>

				</div>
				
				<div class="wrapper style2"><br>

					<article id="main" class="container special">
						<div id="container_barChart" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
						<header>
							<h2 style="color:#ef8376;">議題與銷售量的週期關係</h2>
							<p>
								透過長期議題趨勢，找出企業行銷決策
							</p>
						</header>
						<p>
							
						</p>
						
					</article>

				</div>
				
				<div class="wrapper style2"><br>

					<article id="main" class="container special">
					    <div id="container_lineChart" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
						<header>
							<h2 style="color:#ef8376;">議題詞與週期關係</h2>
							<p>
								議題熱門度的趨勢分析
							</p>
						</header>
						<p>
							
						</p>
						
					</article>

				</div>

			<!-- Features -->

				<section id="features" class="container special">
					<header>
						<h2 style="color:#ef8376;">專案活動與報告分析</h2>
					</header>
					
					
					<div class="wrapper style2"><br>
						<article id="main" class="container special">
						   <a href="#" class="image featured"><img src="images/newgeneration.png" alt="" /></a>
						</article>
			        </div>
					
					<div class="wrapper style2"><br>
						<article id="main" class="container special">
						   <a href="#" class="image featured"><img src="images/iot.png" alt="" /></a>
						</article>
			        </div>
				</section>
				

			<!-- Footer -->
				<?php include("includeFooter.php"); ?>
		</div>	        
	        
    <script type="text/javascript" src="jsonData/dislayCharts.js">
        
    </script>

	</body>
</html>